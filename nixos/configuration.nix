# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  #networking.hostName = "laptop-david"; # Define your hostname.

  networking = {
    hostName = "laptop-david"; # Define your hostname.
    nameservers = [ "1.1.1.1" "8.8.8.8" "8.8.4.4" ];
  };
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Enable Flatpack
  services.flatpak.enable = true;
  system.activationScripts = {
    flathub = ''
      /run/current-system/sw/bin/flatpak remote-add --system --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    '';
  };



# Set your time zone.
  time.timeZone = "America/Sao_Paulo";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "pt_BR.UTF-8";
    LC_IDENTIFICATION = "pt_BR.UTF-8";
    LC_MEASUREMENT = "pt_BR.UTF-8";
    LC_MONETARY = "pt_BR.UTF-8";
    LC_NAME = "pt_BR.UTF-8";
    LC_NUMERIC = "pt_BR.UTF-8";
    LC_PAPER = "pt_BR.UTF-8";
    LC_TELEPHONE = "pt_BR.UTF-8";
    LC_TIME = "pt_BR.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # Add nvidia driver
  # Enable OpenGL
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  # Load nvidia driver for Xorg and Wayland
  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {

    # Modesetting is needed most of the time
    modesetting.enable = true;

    # Enable power management (do not disable this unless you have a reason to).
    # Likely to cause problems on laptops and with screen tearing if disabled.
    powerManagement.enable = true;

    # Use the NVidia open source kernel module (which isn't “nouveau”).
    # Support is limited to the Turing and later architectures. Full list of 
    # supported GPUs is at: 
    # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus 
    # Only available from driver 515.43.04+
    open = false;

    # Enable the Nvidia settings menu,
    # accessible via `nvidia-settings`.
    nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU.
    package = config.boot.kernelPackages.nvidiaPackages.stable;
    prime = {
    # Make sure to use the correct Bus ID values for your system!
      intelBusId = "PCI:00:02:0";
      nvidiaBusId = "PCI:01:0:0";
    };
  };
  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  environment.gnome.excludePackages = (with pkgs; [
    gnome-photos
    gnome-tour
    totem
  ]);

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  virtualisation = {
    docker = {
      enable = true;
      storageDriver = "btrfs";
      rootless = {
        enable = true;
        setSocketVariable = true;
      };
      extraOptions = ''
        --insecure-registry "https://192.168.1.1:5000"
        --data-root "/data"
      '';
    };
  };


  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  users.defaultUserShell = pkgs.zsh;
  programs.zsh.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.david = {
    isNormalUser = true;
    description = "david";
    shell = pkgs.zsh;
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
      firefox
    #  thunderbird
    ];
  };

  # Allow unfree packages
  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = [
      "electron-11.5.0"
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    pkgs.nixos-option
    pkgs.home-manager
    pkgs.rnix-lsp
    pkgs.gitFull
    pkgs.gnome.gnome-software
    pkgs.gnome.gnome-tweaks
    pkgs.nautilus-open-any-terminal
    pkgs.gnome.sushi
    pkgs.vim
    pkgs.wget
    pkgs.blueman
    pkgs.bluez
    pkgs.alacritty
    pkgs.zip
    pkgs.unzip
    pkgs.htop
    pkgs.wget
    pkgs.tree
    pkgs.jq
    pkgs.zsh
    pkgs.bash-completion
    pkgs.mlocate
    pkgs.tilix
    pkgs.google-chrome
    pkgs.gnumake
    pkgs.inetutils
    pkgs.nettools
    pkgs.openvpn
    pkgs.openconnect
    pkgs.networkmanager-openvpn
    pkgs.networkmanager-openconnect
    pkgs.tailscale
    pkgs.wireguard-tools
    pkgs.ulauncher
    pkgs.flameshot
    pkgs.peek
    pkgs.guvcview
    pkgs.clockify
    pkgs.discord
    pkgs.slack
    pkgs.teams
    pkgs.zoom-us
    pkgs.qbittorrent
    pkgs.gparted
    pkgs.timeshift
    pkgs.scrcpy
    pkgs.vlc
    pkgs.libvirt
    pkgs.qemu
    pkgs.virt-manager
    pkgs.spotify
    pkgs.docker
    pkgs.docker-compose
    #pkgs.asdf-vm # Removed to clone repository in home user 
    pkgs.neofetch
    # VSCODE EXTENSIONS
    (vscode-with-extensions.override {
      vscodeExtensions = with vscode-extensions; [
        bbenoist.nix
        jnoortheen.nix-ide
        ms-python.python
        ms-python.vscode-pylance
        ms-azuretools.vscode-docker
        ms-kubernetes-tools.vscode-kubernetes-tools
        ms-vscode-remote.remote-ssh
        ritwickdey.liveserver
        mhutchie.git-graph
        dracula-theme.theme-dracula
        davidanson.vscode-markdownlint
        yzhang.markdown-all-in-one
        esbenp.prettier-vscode
        hashicorp.terraform
        donjayamanne.githistory
        pkief.material-icon-theme
        redhat.vscode-yaml
        golang.go
        johnpapa.vscode-peacock
        gruntfuggly.todo-tree
        njpwerner.autodocstring
        alefragnani.project-manager
        tamasfe.even-better-toml
        streetsidesoftware.code-spell-checker
        sonarsource.sonarlint-vscode
        shardulm94.trailing-spaces
        codezombiech.gitignore
      ];
    })
    # GNOME EXTENSIONS
    pkgs.gnomeExtensions.dash-to-dock
    pkgs.gnomeExtensions.caffeine
    pkgs.gnomeExtensions.openweather
    pkgs.gnomeExtensions.force-quit
    pkgs.gnomeExtensions.coverflow-alt-tab
    pkgs.gnomeExtensions.bluetooth-quick-connect
    pkgs.gnomeExtensions.tiling-assistant
    pkgs.gnomeExtensions.vitals
    pkgs.gnomeExtensions.appindicator

    # SHELL
    pkgs.bat
    pkgs.exa
    pkgs.grex
    pkgs.procs
  ];

  fonts.fonts = with pkgs; [
    nerdfonts
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

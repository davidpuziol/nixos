{ config, lib, pkgs, ... }:

let
  # Set all shell aliases programatically
  shellAliases = {
    k="kubectl";
    dk="docker";
    dc="docker-compose";
    tf="terraform";
    tfd="terraform-docs";
    trp="terraform plan";
    tra="terraform apply";
    ap="ansible-playbook";
    v="nvim";
    ls="ls -lha --color=auto";
    ll = "ls -l --color=auto";
    grep="grep --color=auto";
    ip="ip -color=auto";
    diff="diff --color=auto";
    chat="shell-genie ask";
    update = "sudo nixos-rebuild switch";
    # Rust Shell
    cat = "bat";
    l = "exa";
    # Reload zsh
    szsh = "source ~/.zshrc";
    # Reload zsh
    hms = "home-manager switch";
    # Nix garbage
    garbage = "nix-collect-garbage -d && docker image prune --force";
    # See which Nix packages are installed
    installed = "nix-env --query --installed";
  };

in {

  home = {
    username = "david";
    homeDirectory = "/home/david";
    stateVersion = "23.05";
    sessionVariables = {
      EDITOR = "vim";
      TERMINAL = "alacritty";
    };
  };

  nixpkgs.config = {
    allowUnfree = true;
    allowUnsupportedSystem = true;
  };

  programs.git = {
    enable = true;
    userName = "David";
    userEmail = "davidpuziol@gmail.com";
    extraConfig = {
      core = {
        editor = "vim";
        pager = "delta --dark";
        whitespace = "trailing-space,space-before-tab";
      };
      color = {
        branch = "auto";
        diff = "auto";
        interactive = "auto";
        status = "auto";
      };
    };
    aliases = {
      ec = "git config --global -e";
      co = "checkout";
      cob = "checkout -b";
      c = "commit";
      cm = "commit -m";
      cma = "commit -a --ammend" ;
      lg = "log — all — graph — decorate — oneline — abbrev-commit";
      ac = "!git add -A && git commit -m";
      df = "diff";
      a = "add";
      aa = "add -A";
      tags = "tags -l";
      branches = "branch -a";
      up = "!git pull --rebase --prune $@ && git submodule update --init --recursive";
      save = "!git add -A && git commit -m \"SAVEPOINT\"";
      undo = "reset HEAD~1 --mixed";
      wip = "!git add -A && git commit -qm \"WIPE SAVEPOINT\" && git reset HEAD~1 --hard";
    };
  };

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    inherit shellAliases;
    history = {
      size = 10000;
      save = 10000;
      extended = true;
    };

    # Called whenever zsh is initialized
    initExtra = ''
      export TERM="xterm-256color"
      bindkey -e

      # Nix setup (environment variables, etc.)
      if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then
        . ~/.nix-profile/etc/profile.d/nix.sh
      fi

      # Load environment variables from a file; this approach allows me to not
      # commit secrets like API keys to Git
      if [ -e ~/.env ]; then
        . ~/.env
      fi

      # Start up Starship shell
      # eval "$(starship init zsh)"

      # Autocomplete for various utilities
      source <(terraform completion zsh)
      source <(kubectl completion zsh)
      # source <(linkerd completion zsh)
      # source <(doctl completion zsh)
      # source <(gh completion --shell zsh)
      # rustup completions zsh > ~/.zfunc/_rustup
      # source <(cue completion zsh)
      # source <(npm completion zsh)
      # source <(humioctl completion zsh)
      # source <(fluxctl completion zsh)

      # direnv setup
      # eval "$(direnv hook zsh)"

      # Load asdf
      . $HOME/.asdf/asdf.sh
      autoload -Uz compinit && compinit

      # direnv hook
      # eval "$(direnv hook zsh)"
    '';

    oh-my-zsh = {
      enable = true;
      theme = "powerlevel10k";
      plugins = [
        "git"
        "git-prompt"
        "git-flow"
        "gitfast"
        "golang"
        "rust"
        "python"
        "helm"
        "history"
        "terraform"
        "aws"
        "docker"
        "docker-compose"
        "gcloud"
        "istioctl"
        "jsontools"
        "sudo"
        "asdf"
        "systemd"
        "kubectl"
        "kubectx"
        "vault"
        "vagrant"
        "nomad"
        "pod"
        "ssh-agent"
        "dircycle"
      ];
    };
    zplug = {
      enable = true;
      plugins = [
        # { name = "zsh-users/zsh-autosuggestions"; }
        {
          name = "romkatv/powerlevel10k";
          tags = [
            as:theme depth:1
          ];
        }
      ];
    };
  };
}
# nixos

<img src="./pics/nixlogo.png" alt="drawing" width="250"/>


O repositório oficial do NixOS é <https://github.com/NixOS> é tem muitos contribuidores para o projeto


- Foi criada do zero
- Utiliza o gerenciador de pacotes [nix](https://github.com/NixOS/nix) com uma quantidade de pacote comparável a um AUR. Para procurar pacotes de forma gráfica veja <https://search.nixos.org/packages>
- Possui duas versões stable e unstable
- Releases em maio e novembro
- Sistema de propósito geral, mas especialmente para devops e desenvolvedores devido a capacidade de replicar o ambiente de forma rápida e fácil, pois pode ser configurado de forma declarativa. 
- Permite instalar um pacote temporário só para executar algo sem estar no sistema
- Cria uma forma fácil de rollback, pois tranforma todos os arquivos em link simbólicos.
- Distro iniciada em 2003 e o projeto se mantém por bastante tempo
- Comunidade ativa
- Uiliza systemd

## Por que utilizar o NixOS?

O nix consegue gerar um sistema "imutável" ou perto disso. Tradicionalmente, uma distribuição Linux (ou QUALQUER sistema operacional) funciona de maneira muito semelhante. Todo o seu sistema é, na verdade, apenas uma grande coleção de pacotes reunidos de uma determinada maneira. Instalar e desinstalar coisas vai sujando o sistema e muitas vezes nos deparamos com problemas de dependências entre os pacotes. É trabalho do gerenciado de pacotes manter tudo em ordem mas nem sempre consegue.

A maneira tradicional de se proteger contra isso era fazer backups e instantâneos para os quais você poderia reverter se algo quebrasse. Embora isso seja melhor do que um sistema quebrado, ainda parece um videogame onde você continua volta no checkpoint para tentar mais uma vez.

O Caminho Imutável

A “melhor” maneira de resolver isso é repensar como o sistema é montado. Um sistema operacional imutável possui uma partição central do sistema totalmente protegida e montada como “somente leitura”. Você não pode mexer com isso, mesmo que quisesse. Ao baixar uma atualização, você apenas obtém uma nova imagem grande, troca-a e inicializa na nova. Se algo der errado, basta reverter para a imagem antiga e tentar novamente. Todos os seus arquivos e aplicativos residem em uma partição separada e são pequenos “contêineres” que ficam sobre a imagem base do sistema operacional. Dessa forma, seus aplicativos e arquivos nunca poderão derrubar seu sistema. Um aplicativo pode falhar, mas você sempre terá um sistema funcionando.

Se for um sistema como android, chromeos, ios de um smartphone tá resolvido, mas não no mundo que precisamos de várias ferramentas como docker, kubectl, linguagem de programaćão, clis, etc.

O Fedora saltou de cabeça neste mundo com seu popular projeto Silverblue. Aqui eles estão modelando o estilo de sistema operacional imutável que vimos acima. É apenas uma grande imagem do sistema com coisas preparadas e flatpak como gerenciador de aplicativos na parte superior. Para usuários casuais ou corporativos, isso pode ser tudo de que precisam: um sistema básico sólido com uma coleção de aplicativos flatpak no topo. Para um ambiente que não seja de desenvolvimento funciona, mas vamos instalar linguagens, docker, etc, usando flatpak? E se não tiver? Então vamos executar tudo via container? Eu partiria para outra distro não o silverblue, pois não me sinto muito confortável com isso, apesar de funcionar. Quero instalar as coisas no sistema. É como comprar um carro com banco de couro e colocar uma capa.

Para se proteger disso recorremos aos backups que são sempre necessários independente do uso do Nix, mas a idéia é ter backup por causa da informaćão que você precisa, e não para um rollback de pacotes.

## Arquivo de configuraćão

Sobre o arquivo de configuraćão ele faz a configuraćao de como o sistema deve ser, quais pacotes tem instalados, configuraćões do sistema, etc.

Esse arquivo fica em **/etc/nixos/configuration.nix**

Você irá encontrar já um arquivo base, mas para encontrar mais configuraćões para serem colocadas, você pode utilizar o site <https://search.nixos.org/options> e digitar o que procura que ele vai te dar o caminho.

Um outro jeito de encontrar navegar entre as općões é instalar o nix-options no sistema que ajudará a navegar entre as općões.

O terceiro método e mais eficaz é montar um ambiente no vscode, principalmente se vc já estiver usando o Nix, que facilitará a edićão do arquivo usando o language server. Para isso instale a extensão https://marketplace.visualstudio.com/items?itemName=pinage404.nix-extension-pack, coloque no seu `environment.systePackages` o pkgs.rnix-lsp como manda a extensão e dentro do settings.json do vscode a linha `"nix.enableLanguageServer": true`. Copie o configuration.nix para algum lugar e comeće a trabalhar nele pelo vscode.

Porém sendo mais um pouco esperto, vamos preparar um ambiente para desenvolver esse arquivo no vscode. Vamos utilizar a extensão

vamos montar um ambiente para desenvolver esse arquivo.

A primeira coisa que irá te ajudar é instalar o pacote nixos-option que ajuda na navegar de configuraćão para descobrir o que é e o que não é possível fazer.

tenha instalado no sistema tb o rnix-lsp

e no habilite a extensão no vscode 


 mais tem algumas limitacoes, por exemplo:
Voce pode habilitar o flatpak no sistema, mas adicionar um repositório é uma funćão do commando flatpak e não necessáriamente do sistema.

Ai vem a pergunta o sistema precisa ainda de um ansible por tras para resolver esses problemas?





## Configurando seu home automaticamente



- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/davidpuziol/nixos.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/davidpuziol/nixos/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
